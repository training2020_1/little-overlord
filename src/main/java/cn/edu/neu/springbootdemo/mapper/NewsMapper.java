package cn.edu.neu.springbootdemo.mapper;

import org.apache.ibatis.annotations.Mapper;

import cn.edu.neu.springbootdemo.model.News;

@Mapper
public interface NewsMapper {

	  int addNews(News news);

}
