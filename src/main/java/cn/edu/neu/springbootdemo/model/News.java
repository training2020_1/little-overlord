package cn.edu.neu.springbootdemo.model;

import java.sql.Date;

public class News {
	private int newsid;
	private int userid;
	private String content;
	private String createtime;
	private String firstpicture;
	private String title;
	public News() {}
	public int getNewsid() {
		return newsid;
	}
	public void setNewsid(int newsid) {
		this.newsid = newsid;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	
	public String getFirstpicture() {
		return firstpicture;
	}
	public void setFirstpicture(String firstpicture) {
		this.firstpicture = firstpicture;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Override
	public String toString() {
		return "News [newsid=" + newsid + ", userid=" + userid + ", content=" + content + ", createtime=" + createtime
				+ ", firstpicture=" + firstpicture + ", title=" + title + "]";
	};
	
	
	
}
