package cn.edu.neu.springbootdemo.service;

import java.util.List;

import cn.edu.neu.springbootdemo.core.common.Page;
import cn.edu.neu.springbootdemo.model.User;

public interface UserService {

	User existsUser(User user);

	List<User> getUserList(User user);

	boolean deleteUser(User user);

	int addUser(User user);

	User getUser(User user);

	boolean updateUser(User user);


}
