package cn.edu.neu.springbootdemo.service;

import cn.edu.neu.springbootdemo.model.News;

public interface NewsService {

	boolean addNews(News news);

}
