package cn.edu.neu.springbootdemo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.springbootdemo.mapper.NewsMapper;
import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.service.NewsService;

@Service
public class NewsServiceImpl implements NewsService {
	
	@Autowired
	NewsMapper nm;
	
	
	@Override
	public boolean addNews(News news) {
		// TODO Auto-generated method stub
		int result = nm.addNews(news);
		return false;
	}

}
