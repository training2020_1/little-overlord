package cn.edu.neu.springbootdemo.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/")
public class HomeAction {
	@RequestMapping("/")
	public String home(){
		return "/index2";
	}
	
	@RequestMapping("/login")
	public String login(){
		return "/login";
	}
}
