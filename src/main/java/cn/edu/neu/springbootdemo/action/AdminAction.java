package cn.edu.neu.springbootdemo.action;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.neu.springbootdemo.core.Constants;
import cn.edu.neu.springbootdemo.model.User;
import cn.edu.neu.springbootdemo.service.AdminService;

@Controller
@RequestMapping("/admin")
public class AdminAction {

	@Autowired
	AdminService as;
	
	
	@RequestMapping("/gotoAboutMe")
	public String gotoAboutMe(HttpSession session)
	{
		User user = (User) session.getAttribute(Constants.LOGIN_USER);
		session.setAttribute("username", user.getUsername());
		System.out.println(user);
		return "/admin/blogs";
	}
	@RequestMapping("gotoInput")
	public String gotoInput()
	{
		return "/admin/blogs-input";
	}
	@RequestMapping("gotoHome")
	public String gotoHome()
	{
		return "/index2";
	}
	
	@RequestMapping("/input")
	public String input()
	{
		return "/input";
	}
}
