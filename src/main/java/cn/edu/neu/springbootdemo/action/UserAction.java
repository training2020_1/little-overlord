package cn.edu.neu.springbootdemo.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.neu.springbootdemo.core.Constants;
import cn.edu.neu.springbootdemo.core.common.Page;
import cn.edu.neu.springbootdemo.model.User;
import cn.edu.neu.springbootdemo.service.UserService;


@Controller
@RequestMapping("/user")
public class UserAction {
	@Autowired
	UserService userService;
	
	@RequestMapping("/getUserList")
	public String getUserList(Model model,User user) {
		//model存记录
//		List<User> userList=userService.getUserList();
		System.out.println("user:"+user);
		List<User> userList=userService.getUserList(user);
		model.addAttribute("userList",userList);
		return "/user/userList";
	}
	
	@RequestMapping("/deleteUser")
	public String deleteUser(User user,Model model) {
		boolean f=userService.deleteUser(user);
		if(f) {
			return "forward:/user/getUserList";
		}
		else {
			model.addAttribute("result","no");
			model.addAttribute("msg","删除失败");
			
			return "/user/result";
			
		}
	}
	
	@RequestMapping("/addUser")
	public String addUser(User user,Model model) {
		int i=userService.addUser(user);
		if(i==0) {
			model.addAttribute("result","yes");
			model.addAttribute("msg","恭喜您，注册成功");
		}
		else if(i==1) {
			model.addAttribute("result","no");
			model.addAttribute("msg","对不起，用户名已存在");
		}
		else if(i==2) {
			model.addAttribute("result","no");
			model.addAttribute("msg","对不起，注册失败");	
		}
		return "/user/result";
	}
	
	@RequestMapping("/getUser")
	public String getUser(User user,Model model) {
		User u=userService.getUser(user);
		model.addAttribute("user",u);
		return "/user/edit";
	}
	@RequestMapping("/updateUser")
	public String updateUser(User user,Model model) {
		boolean f=userService.updateUser(user);
		if(f) {
			model.addAttribute("result","yes");
			model.addAttribute("msg","修改成功");
		}
		else {
			model.addAttribute("result","no");
			model.addAttribute("msg","修改失败");
			
		}
		return "/user/result";
	}
	
//	@RequestMapping("/getUserListWithPage")
//	public String getUserListWithPage(User user,Model model) {
//		Page<User> page=userService.getUserListWithPage(user);
//		model.addAttribute("page",page);
//		return "/user/userList";
//		
//	}
	
	@RequestMapping("/login") 
	public String login(User user,HttpSession session,Map<String,String> m){
		System.out.println("userlogin:"+user);
		User dbUser=userService.existsUser(user);
		//System.out.println(user.getUserName()+","+user.getPassword()+"--------"+user1);
		if(dbUser!=null){		
			session.setAttribute(Constants.LOGIN_USER, dbUser);
			m.put("login", "yes");	
			String redirUrl=(String)session.getAttribute(Constants.ORIGINAL_URL);
			System.out.println("redirUrl:"+redirUrl);
			return "redirect:"+redirUrl;
		}
		else{
			m.put("login",Constants.LOGIN_ERR);	
			return "/login";
		}
		 
	}
	//注销
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.removeAttribute(Constants.LOGIN_USER);
		
		
		return "/index2";
	}
	
}
