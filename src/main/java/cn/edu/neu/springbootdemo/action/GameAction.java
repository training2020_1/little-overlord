package cn.edu.neu.springbootdemo.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/game")
public class GameAction {

	@RequestMapping("/gameList")
	public String getGameList() {

		return "/game/gameList";
	}
	}//