package cn.edu.neu.springbootdemo.action;
import java.util.Date;

import javax.servlet.http.HttpSession;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.neu.springbootdemo.core.Constants;
import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.model.User;
import cn.edu.neu.springbootdemo.service.NewsService;

@Controller
@RequestMapping("/news")
public class NewsAction {
	
	@Autowired
	NewsService ns;
	
	
	
	@RequestMapping("/input")
	public String input(News news, HttpSession session)
	{
		User user = (User) session.getAttribute(Constants.LOGIN_USER);
		System.out.println("###");
		System.out.println(user);
		System.out.println("###");
		System.out.println(news);
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = format.format(date);	
		news.setCreatetime(dateStr);
		news.setUserid(user.getUserid());
		boolean result = ns.addNews(news);
		return "/admin/blogs";
		
	}
	
	
}
